resource "null_resource" "inventory"{
    depends_on = [yandex_compute_instance.node1, yandex_compute_instance.node2, yandex_compute_instance.srv]

    provisioner "local-exec" {
        command = "echo '[all]' > $INI;echo 'node1 ansible_host='$N1N' ip='$N1L >> $INI;echo 'node2 ansible_host='$N2N' ip='$N2L >> $INI;echo 'srv ansible_host='$SN' ip='$SL >> $INI;echo '[kube_control_plane]' >> $INI;echo 'node1' >> $INI;echo '[etcd]' >> $INI;echo 'node1' >> $INI; echo '[kube_node]' >> $INI;echo 'node1' >> $INI;echo 'node2' >> $INI;echo '[calico_rr]' >> $INI;echo '[k8s_cluster:children]' >> $INI;echo 'kube_control_plane' >> $INI;echo 'kube_node' >> $INI;echo 'calico_rr' >> $INI;echo $N1N > /tmp/node1.txt"
        environment = {
          INI = "./inventory"
          N1L = "${yandex_compute_instance.node1.network_interface.0.ip_address}"
          N1N = "${yandex_compute_instance.node1.network_interface.0.nat_ip_address}"
          N2L = "${yandex_compute_instance.node2.network_interface.0.ip_address}"
          N2N = "${yandex_compute_instance.node2.network_interface.0.nat_ip_address}"
          SL = "${yandex_compute_instance.srv.network_interface.0.ip_address}"
          SN = "${yandex_compute_instance.srv.network_interface.0.nat_ip_address}"
         }
    }

    provisioner "local-exec"{
        when = destroy
        command = "rm ./inventory"
    }
}

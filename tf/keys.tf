variable "yc_credentials" {
  description = "Credentials for deploying Terraform"
  type        = object({
    token     = string
    cloud_id  = string
    folder_id = string
  })
  default     = {
    token     = ""
    cloud_id  = ""
    folder_id = ""
  }
}


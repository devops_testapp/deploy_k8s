#!/bin/bash
cd tf && terraform init && echo yes | terraform apply && sleep 30 && cd ..
git clone https://github.com/kubernetes-sigs/kubespray.git -b release-2.20
VENVDIR=kubespray-venv
KUBESPRAYDIR=kubespray
ANSIBLE_VERSION=2.12
virtualenv  --python=$(which python3) $VENVDIR
source $VENVDIR/bin/activate
cd $KUBESPRAYDIR
pip install -U -r requirements-$ANSIBLE_VERSION.txt
cp -rfp inventory/sample inventory/testapp && cd ..
sed -i "s/supplementary_addresses_in_ssl_keys: .*/supplementary_addresses_in_ssl_keys: [$(cat /tmp/node1.txt)]/" ./k8s-cluster.yml
cp -r k8s-cluster.yml ./kubespray/inventory/testapp/group_vars/k8s_cluster/k8s-cluster.yml
cp -r addons.yml ./kubespray/inventory/testapp/group_vars/k8s_cluster/addons.yml
cp -r tf/inventory ./kubespray/inventory/testapp/inventory.ini
cd kubespray
ansible-playbook -i inventory/testapp/inventory.ini -u ubuntu -b -v --private-key=~/.ssh/id_rsa cluster.yml
cd .. && ansible-playbook --extra-vars "ansible_ssh_common_args='-o StrictHostKeyChecking=no'" -i tf/inventory -b -v -u ubuntu runner.yaml --ask-vault-pass
kubectl create namespace testapp && kubectl create namespace monitoring